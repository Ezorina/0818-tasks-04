#include <stdio.h>
#define minVal 2
#define maxVal 1000000

int KollatzNum(unsigned long long int num)
{
	int step = 0;
	while (num > 1)
	{
		if (num % 2 == 0)
		{
			num = num / 2;
			step++;
		}
		else
		{
			num = 3 * num + 1;
			step++;
		}
	}
	return step;
}

int main()
{
	char str[50];
	char temp;
	unsigned long long int num = minVal;
	int steps = 0, maxNum = 0, maxSteps = 0;

	while(num <= maxVal)
	{
		steps = KollatzNum(num);
		if(steps > maxSteps)
		{
			maxNum = num;
			maxSteps = steps;
		}
		num++;
	}
	
	printf("%d", maxNum);
	return 0;
}